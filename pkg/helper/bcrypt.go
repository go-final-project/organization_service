package helper

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

func GetBcryptPassword(password string) (string, error) {
	pass := []byte(password)

	hash, err := bcrypt.GenerateFromPassword(pass, bcrypt.DefaultCost)

	return string(hash), err
}

func ComparePassword(hashedPassword, password string) bool {

	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		fmt.Println(hashedPassword, password)
		return false
	}
	return true
}
