package config

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"

	TimeExpiredAt = time.Hour * 800
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	PostgresHost     string
	PostgresPort     int
	PostgresUser     string
	PostgresPassword string
	PostgresDatabase string

	OrganizationServiceHost string
	OrganizationGRPCPort    string

	ProductServiceHost string
	ProductGRPCPort    string

	StockServiceHost string
	StockGRPCPort    string

	KassaServiceHost string
	KassaGRPCPort    string

	PostgresMaxConnections int32

	SecretKey string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("./.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "organization_service"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.PostgresHost = cast.ToString(getOrReturnDefaultValue("POSTGRES_HOST", "localhost"))
	config.PostgresPort = cast.ToInt(getOrReturnDefaultValue("POSTGRES_PORT", 5432))
	config.PostgresUser = cast.ToString(getOrReturnDefaultValue("POSTGRES_USER", "postgres"))
	config.PostgresPassword = cast.ToString(getOrReturnDefaultValue("POSTGRES_PASSWORD", "123123"))
	config.PostgresDatabase = cast.ToString(getOrReturnDefaultValue("POSTGRES_DATABASE", "organization_service"))

	config.PostgresMaxConnections = cast.ToInt32(getOrReturnDefaultValue("POSTGRES_MAX_CONNECTIONS", 30))

	config.OrganizationServiceHost = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_SERVICE_HOST", "localhost"))
	config.OrganizationGRPCPort = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_GRPC_PORT", ":9101"))

	config.ProductServiceHost = cast.ToString(getOrReturnDefaultValue("PRODUCT_SERVICE_HOST", "localhost"))
	config.ProductGRPCPort = cast.ToString(getOrReturnDefaultValue("PRODUCT_GRPC_PORT", ":9102"))

	config.StockServiceHost = cast.ToString(getOrReturnDefaultValue("STOCK_SERVICE_HOST", "localhost"))
	config.StockGRPCPort = cast.ToString(getOrReturnDefaultValue("STOCK_GRPC_PORT", ":9103"))

	config.KassaServiceHost = cast.ToString(getOrReturnDefaultValue("KASSA_SERVICE_HOST", "localhost"))
	config.KassaGRPCPort = cast.ToString(getOrReturnDefaultValue("KASSA_GRPC_PORT", ":9104"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "secret"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
