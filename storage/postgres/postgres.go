package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/GoFinalProject/organization_service/config"
	"gitlab.com/GoFinalProject/organization_service/storage"
)

type Store struct {
	db           *pgxpool.Pool
	branch       *BranchRepo
	employee     *EmployeeRepo
	provider     *ProviderRepo
	salesAddress *SalesAddressRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch
}

func (s *Store) Provider() storage.ProviderRepoI {
	if s.provider == nil {
		s.provider = NewProviderRepo(s.db)
	}

	return s.provider
}

func (s *Store) Employee() storage.EmployeeRepoI {
	if s.employee == nil {
		s.employee = NewEmployeeRepo(s.db)
	}

	return s.employee
}

func (s *Store) SalesAddress() storage.SalesAddressRepoI {
	if s.salesAddress == nil {
		s.salesAddress = NewSalesAddressRepo(s.db)
	}

	return s.salesAddress
}
