package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type EmployeeRepo struct {
	db *pgxpool.Pool
}

func NewEmployeeRepo(db *pgxpool.Pool) *EmployeeRepo {
	return &EmployeeRepo{
		db: db,
	}
}

func (r *EmployeeRepo) Create(ctx context.Context, req *organization_service.EmployeeCreate) (*organization_service.Employee, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO employees(id, first_name, last_name, phone, login, password, user_type)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	psw, err := helper.GetBcryptPassword(req.Password)
	_, err = r.db.Exec(ctx, query,
		id,
		req.FirstName,
		req.LastName,
		req.PhoneNumber,
		req.Login,
		psw,
		req.UserType,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Employee{
		Id:          id,
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		UserType:    req.UserType,
	}, nil
}

func (r *EmployeeRepo) GetByID(ctx context.Context, req *organization_service.EmployeePrimaryKey) (*organization_service.Employee, error) {
	var (
		where = " WHERE id = $1"
		query string

		id        sql.NullString
		firstName string
		lastName  string
		phone     string
		login     string
		password  string
		userType  string
	)

	query = `
		SELECT
			id, first_name, last_name, phone, login, password, user_type
		FROM employees
	`
	if len(req.Id) > 0 {
		where = " WHERE id = $1"
	} else {
		where = " WHERE login = $1"
		req.Id = req.Login
	}

	query += where

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&firstName,
		&lastName,
		&phone,
		&login,
		&password,
		&userType,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Employee{
		Id:          id.String,
		FirstName:   firstName,
		LastName:    lastName,
		PhoneNumber: phone,
		Login:       login,
		Password:    password,
		UserType:    userType,
	}, nil
}

func (r *EmployeeRepo) GetList(ctx context.Context, req *organization_service.EmployeeGetListRequest) (*organization_service.EmployeeGetListResponse, error) {

	var (
		resp   = &organization_service.EmployeeGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, first_name, last_name, phone, user_type	
		FROM employees
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByFirstName != "" {
		where += ` AND first_name ILIKE '%' || '` + req.SearchByFirstName + `' || '%'`
	}
	if req.SearchByLastName != "" {
		where += ` AND last_name ILIKE '%' || '` + req.SearchByLastName + `' || '%'`
	}
	if req.SearchByPhoneNumber != "" {
		where += ` AND phone ILIKE '%' || '` + req.SearchByPhoneNumber + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			firstName string
			lastName  string
			phone     string
			userType  string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&firstName,
			&lastName,
			&phone,
			&userType,
		)

		if err != nil {
			return nil, err
		}

		resp.Employees = append(resp.Employees, &organization_service.Employee{
			Id:          id.String,
			FirstName:   firstName,
			LastName:    lastName,
			PhoneNumber: phone,
			UserType:    userType,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *EmployeeRepo) Update(ctx context.Context, req *organization_service.EmployeeUpdate) (*organization_service.Employee, error) {

	var (
		query  string
		params map[string]interface{}
	)
	query = `
		UPDATE
			employees
		SET
		    first_name = :first_name,
		    last_name = :last_name,
		    phone = :phone,
		    login = :login,
			password = :password,
			user_type = :user_type
		WHERE id = :id
	`
	psw, err := helper.GetBcryptPassword(req.Password)
	params = map[string]interface{}{
		"id":         req.Id,
		"first_name": req.FirstName,
		"last_name":  req.LastName,
		"phone":      req.PhoneNumber,
		"login":      req.Login,
		"password":   psw,
		"user_type":  req.UserType,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Employee{
		Id:          req.Id,
		FirstName:   req.FirstName,
		LastName:    req.LastName,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		UserType:    req.UserType,
	}, nil
}

func (r *EmployeeRepo) Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM employees WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
