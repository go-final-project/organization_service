package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type SalesAddressRepo struct {
	db *pgxpool.Pool
}

func NewSalesAddressRepo(db *pgxpool.Pool) *SalesAddressRepo {
	return &SalesAddressRepo{
		db: db,
	}
}

func (r *SalesAddressRepo) Create(ctx context.Context, req *organization_service.SalesAddressCreate) (*organization_service.SalesAddress, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO sales_address(id, name, branch_id)
		VALUES ($1, $2, $3)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.BranchId,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.SalesAddress{
		Id:       id,
		Name:     req.Name,
		BranchId: req.BranchId,
	}, nil
}

func (r *SalesAddressRepo) GetByID(ctx context.Context, req *organization_service.SalesAddressPrimaryKey) (*organization_service.SalesAddress, error) {
	var (
		query string

		id       sql.NullString
		name     string
		branchId string
	)

	query = `
		SELECT
			id, name, branch_id	
		FROM sales_address
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branchId,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.SalesAddress{
		Id:       id.String,
		Name:     name,
		BranchId: branchId,
	}, nil
}

func (r *SalesAddressRepo) GetList(ctx context.Context, req *organization_service.SalesAddressGetListRequest) (*organization_service.SalesAddressGetListResponse, error) {

	var (
		resp   = &organization_service.SalesAddressGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, name, branch_id	
		FROM sales_address
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByName != "" {
		where += ` AND name ILIKE '%' || '` + req.SearchByName + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id       sql.NullString
			name     string
			branchId string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branchId,
		)

		if err != nil {
			return nil, err
		}

		resp.Addresses = append(resp.Addresses, &organization_service.SalesAddress{
			Id:       id.String,
			Name:     name,
			BranchId: branchId,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *SalesAddressRepo) Update(ctx context.Context, req *organization_service.SalesAddressUpdate) (*organization_service.SalesAddress, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			sales_address
		SET
			name = :name,
			branch_id = :branch_id
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.Id,
		"name":      req.Name,
		"branch_id": req.BranchId,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.SalesAddress{
		Id:       req.Id,
		Name:     req.Name,
		BranchId: req.BranchId,
	}, nil
}

func (r *SalesAddressRepo) Delete(ctx context.Context, req *organization_service.SalesAddressPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM sales_address WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
