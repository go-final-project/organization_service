package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ProviderRepo struct {
	db *pgxpool.Pool
}

func NewProviderRepo(db *pgxpool.Pool) *ProviderRepo {
	return &ProviderRepo{
		db: db,
	}
}

func (r *ProviderRepo) Create(ctx context.Context, req *organization_service.ProviderCreate) (*organization_service.Provider, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO provider(id, name, phone, active)
		VALUES ($1, $2, $3, $4)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Name,
		req.Active,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Provider{
		Id:          id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Active:      req.Active,
	}, nil
}

func (r *ProviderRepo) GetByID(ctx context.Context, req *organization_service.ProviderPrimaryKey) (*organization_service.Provider, error) {
	var (
		query string

		id     sql.NullString
		name   string
		phone  string
		active string
	)

	query = `
		SELECT
			id, name, phone, active
		FROM provider
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&phone,
		&active,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Provider{
		Id:          id.String,
		Name:        name,
		PhoneNumber: phone,
		Active:      active,
	}, nil
}

func (r *ProviderRepo) GetList(ctx context.Context, req *organization_service.ProviderGetListRequest) (*organization_service.ProviderGetListResponse, error) {

	var (
		resp   = &organization_service.ProviderGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, name, phone, active
		FROM provider
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByName != "" {
		where += ` AND name ILIKE '%' || '` + req.SearchByName + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id     sql.NullString
			name   string
			phone  string
			active string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&phone,
			&active,
		)

		if err != nil {
			return nil, err
		}

		resp.Providers = append(resp.Providers, &organization_service.Provider{
			Id:          id.String,
			Name:        name,
			PhoneNumber: phone,
			Active:      active,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ProviderRepo) Update(ctx context.Context, req *organization_service.ProviderUpdate) (*organization_service.Provider, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			provider
		SET
			name = :name,
			phone = :phone,
			active = :active
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":     req.Id,
		"name":   req.Name,
		"phone":  req.PhoneNumber,
		"active": req.Active,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Provider{
		Id:          req.Id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Active:      req.Active,
	}, nil
}

func (r *ProviderRepo) Delete(ctx context.Context, req *organization_service.ProviderPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM provider WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
