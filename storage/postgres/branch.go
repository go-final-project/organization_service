package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/pkg/helper"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *organization_service.BranchCreate) (*organization_service.Branch, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO branch(id, code, name, address, phone)
		VALUES ($1, $2, $3, $4, $5)
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BranchCode,
		req.Name,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          id,
		BranchCode:  req.BranchCode,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) GetByID(ctx context.Context, req *organization_service.BranchPrimaryKey) (*organization_service.Branch, error) {
	var (
		query string

		id      sql.NullString
		code    string
		name    string
		address string
		phone   string
	)

	query = `
		SELECT
			id, code, name, address, phone	
		FROM branch
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&code,
		&name,
		&address,
		&phone,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          id.String,
		BranchCode:  code,
		Name:        name,
		Address:     address,
		PhoneNumber: phone,
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error) {

	var (
		resp   = &organization_service.BranchGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(), id, code, name, address, phone	
		FROM branch
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByName != "" {
		where += ` AND name ILIKE '%' || '` + req.SearchByName + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id      sql.NullString
			code    string
			name    string
			address string
			phone   string
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&code,
			&name,
			&address,
			&phone,
		)

		if err != nil {
			return nil, err
		}

		resp.Branches = append(resp.Branches, &organization_service.Branch{
			Id:          id.String,
			BranchCode:  code,
			Name:        name,
			Address:     address,
			PhoneNumber: phone,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *organization_service.BranchUpdate) (*organization_service.Branch, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			branch
		SET
			code = :photo,
			name = :name,
			address = :address,
			phone = :phone
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":      req.Id,
		"code":    req.BranchCode,
		"name":    req.Name,
		"address": req.Address,
		"phone":   &req.PhoneNumber,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Branch{
		Id:          req.Id,
		BranchCode:  req.BranchCode,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM branch WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
