package storage

import (
	"context"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Provider() ProviderRepoI
	Employee() EmployeeRepoI
	SalesAddress() SalesAddressRepoI
}

type BranchRepoI interface {
	Create(context.Context, *organization_service.BranchCreate) (*organization_service.Branch, error)
	GetByID(context.Context, *organization_service.BranchPrimaryKey) (*organization_service.Branch, error)
	GetList(context.Context, *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error)
	Update(context.Context, *organization_service.BranchUpdate) (*organization_service.Branch, error)
	Delete(context.Context, *organization_service.BranchPrimaryKey) error
}

type ProviderRepoI interface {
	Create(context.Context, *organization_service.ProviderCreate) (*organization_service.Provider, error)
	GetByID(context.Context, *organization_service.ProviderPrimaryKey) (*organization_service.Provider, error)
	GetList(context.Context, *organization_service.ProviderGetListRequest) (*organization_service.ProviderGetListResponse, error)
	Update(context.Context, *organization_service.ProviderUpdate) (*organization_service.Provider, error)
	Delete(context.Context, *organization_service.ProviderPrimaryKey) error
}

type EmployeeRepoI interface {
	Create(context.Context, *organization_service.EmployeeCreate) (*organization_service.Employee, error)
	GetByID(context.Context, *organization_service.EmployeePrimaryKey) (*organization_service.Employee, error)
	GetList(context.Context, *organization_service.EmployeeGetListRequest) (*organization_service.EmployeeGetListResponse, error)
	Update(context.Context, *organization_service.EmployeeUpdate) (*organization_service.Employee, error)
	Delete(context.Context, *organization_service.EmployeePrimaryKey) error
}

type SalesAddressRepoI interface {
	Create(context.Context, *organization_service.SalesAddressCreate) (*organization_service.SalesAddress, error)
	GetByID(context.Context, *organization_service.SalesAddressPrimaryKey) (*organization_service.SalesAddress, error)
	GetList(context.Context, *organization_service.SalesAddressGetListRequest) (*organization_service.SalesAddressGetListResponse, error)
	Update(context.Context, *organization_service.SalesAddressUpdate) (*organization_service.SalesAddress, error)
	Delete(context.Context, *organization_service.SalesAddressPrimaryKey) error
}
