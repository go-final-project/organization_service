package grpc

import (
	"gitlab.com/GoFinalProject/organization_service/config"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/grpc/client"
	"gitlab.com/GoFinalProject/organization_service/grpc/service"
	"gitlab.com/GoFinalProject/organization_service/pkg/logger"
	"gitlab.com/GoFinalProject/organization_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	organization_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	organization_service.RegisterEmployeeServiceServer(grpcServer, service.NewEmployeeService(cfg, log, strg, srvc))
	organization_service.RegisterProviderServiceServer(grpcServer, service.NewProviderService(cfg, log, strg, srvc))
	organization_service.RegisterSalesAddressServiceServer(grpcServer, service.NewSalesAddressService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
