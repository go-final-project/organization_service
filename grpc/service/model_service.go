package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/organization_service/config"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/grpc/client"
	"gitlab.com/GoFinalProject/organization_service/pkg/logger"
	"gitlab.com/GoFinalProject/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SalesAddressService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedSalesAddressServiceServer
}

func NewSalesAddressService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SalesAddressService {
	return &SalesAddressService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SalesAddressService) Create(ctx context.Context, req *organization_service.SalesAddressCreate) (*organization_service.SalesAddress, error) {
	u.log.Info("====== SalesAddress Create ======", logger.Any("req", req))

	resp, err := u.strg.SalesAddress().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create SalesAddress: u.strg.SalesAddress().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SalesAddressService) GetById(ctx context.Context, req *organization_service.SalesAddressPrimaryKey) (*organization_service.SalesAddress, error) {
	u.log.Info("====== SalesAddress Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.SalesAddress().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While SalesAddress Get By ID: u.strg.SalesAddress().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SalesAddressService) GetList(ctx context.Context, req *organization_service.SalesAddressGetListRequest) (*organization_service.SalesAddressGetListResponse, error) {
	u.log.Info("====== SalesAddress Get List ======", logger.Any("req", req))

	resp, err := u.strg.SalesAddress().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While SalesAddress Get List: u.strg.SalesAddress().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SalesAddressService) Update(ctx context.Context, req *organization_service.SalesAddressUpdate) (*organization_service.SalesAddress, error) {
	u.log.Info("====== SalesAddress Update ======", logger.Any("req", req))

	resp, err := u.strg.SalesAddress().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While SalesAddress Update: u.strg.SalesAddress().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SalesAddressService) Delete(ctx context.Context, req *organization_service.SalesAddressPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== SalesAddress Delete ======", logger.Any("req", req))

	err := u.strg.SalesAddress().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While SalesAddress Delete: u.strg.SalesAddress().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
