package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/GoFinalProject/organization_service/config"
	"gitlab.com/GoFinalProject/organization_service/genproto/organization_service"
	"gitlab.com/GoFinalProject/organization_service/grpc/client"
	"gitlab.com/GoFinalProject/organization_service/pkg/logger"
	"gitlab.com/GoFinalProject/organization_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type EmployeeService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedEmployeeServiceServer
}

func NewEmployeeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *EmployeeService {
	return &EmployeeService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *EmployeeService) Create(ctx context.Context, req *organization_service.EmployeeCreate) (*organization_service.Employee, error) {
	u.log.Info("====== Employee Create ======", logger.Any("req", req))

	resp, err := u.strg.Employee().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Employee: u.strg.Employee().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *EmployeeService) GetById(ctx context.Context, req *organization_service.EmployeePrimaryKey) (*organization_service.Employee, error) {
	u.log.Info("====== Employee Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Employee().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Employee Get By ID: u.strg.Employee().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *EmployeeService) GetList(ctx context.Context, req *organization_service.EmployeeGetListRequest) (*organization_service.EmployeeGetListResponse, error) {
	u.log.Info("====== Employee Get List ======", logger.Any("req", req))

	resp, err := u.strg.Employee().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Employee Get List: u.strg.Employee().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *EmployeeService) Update(ctx context.Context, req *organization_service.EmployeeUpdate) (*organization_service.Employee, error) {
	u.log.Info("====== Employee Update ======", logger.Any("req", req))

	resp, err := u.strg.Employee().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Employee Update: u.strg.Employee().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *EmployeeService) Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Employee Delete ======", logger.Any("req", req))

	err := u.strg.Employee().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Employee Delete: u.strg.Employee().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
