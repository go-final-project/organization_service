package client

import (
	"gitlab.com/GoFinalProject/organization_service/config"
	"gitlab.com/GoFinalProject/organization_service/genproto/kassa_service"
	"gitlab.com/GoFinalProject/organization_service/genproto/product_service"
	"gitlab.com/GoFinalProject/organization_service/genproto/stock_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Product service
	BrandService() product_service.BrandServiceClient
	CategoryService() product_service.CategoryServiceClient
	ProductService() product_service.ProductServiceClient

	// Stock service
	ComingProductService() stock_service.ComingProductServiceClient
	ComingService() stock_service.ComingServiceClient
	RemainingService() stock_service.RemainingServiceClient

	// Kassa service
	SaleService() kassa_service.SaleServiceClient
	SaleProductService() kassa_service.SaleProductServiceClient
	SmenaService() kassa_service.SmenaServiceClient
	TransactionService() kassa_service.TransactionServiceClient
	PaymentService() kassa_service.PaymentServiceClient
}

type grpcClients struct {
	// Product service
	brandService    product_service.BrandServiceClient
	categoryService product_service.CategoryServiceClient
	productService  product_service.ProductServiceClient

	// Stock service
	comingProductService stock_service.ComingProductServiceClient
	comingService        stock_service.ComingServiceClient
	remainingService     stock_service.RemainingServiceClient

	// Kassa service
	saleService        kassa_service.SaleServiceClient
	saleProductService kassa_service.SaleProductServiceClient
	smenaService       kassa_service.SmenaServiceClient
	transactionService kassa_service.TransactionServiceClient
	paymentService     kassa_service.PaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Product service
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Stock service
	connStockService, err := grpc.Dial(
		cfg.StockServiceHost+cfg.StockGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	// Kassa service
	connKassaService, err := grpc.Dial(
		cfg.KassaServiceHost+cfg.KassaGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Product service
		brandService:    product_service.NewBrandServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),
		productService:  product_service.NewProductServiceClient(connProductService),

		// Stock service
		comingProductService: stock_service.NewComingProductServiceClient(connStockService),
		comingService:        stock_service.NewComingServiceClient(connStockService),
		remainingService:     stock_service.NewRemainingServiceClient(connStockService),

		// Kassa service
		saleService:        kassa_service.NewSaleServiceClient(connKassaService),
		saleProductService: kassa_service.NewSaleProductServiceClient(connKassaService),
		smenaService:       kassa_service.NewSmenaServiceClient(connKassaService),
		transactionService: kassa_service.NewTransactionServiceClient(connKassaService),
		paymentService:     kassa_service.NewPaymentServiceClient(connKassaService),
	}, nil
}
func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) ComingProductService() stock_service.ComingProductServiceClient {
	return g.comingProductService
}

func (g *grpcClients) ComingService() stock_service.ComingServiceClient {
	return g.comingService
}

func (g *grpcClients) RemainingService() stock_service.RemainingServiceClient {
	return g.remainingService
}

func (g *grpcClients) SaleService() kassa_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() kassa_service.SaleProductServiceClient {
	return g.saleProductService
}

func (g *grpcClients) SmenaService() kassa_service.SmenaServiceClient {
	return g.smenaService
}

func (g *grpcClients) TransactionService() kassa_service.TransactionServiceClient {
	return g.transactionService
}

func (g *grpcClients) PaymentService() kassa_service.PaymentServiceClient {
	return g.paymentService
}
