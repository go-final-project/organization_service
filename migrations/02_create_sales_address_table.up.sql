CREATE TABLE sales_address (
    id UUID PRIMARY KEY,
    name VARCHAR NOT NULL,
    branch_id  UUID REFERENCES branch("id")
);