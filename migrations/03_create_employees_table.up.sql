CREATE TABLE employees (
    id UUID PRIMARY KEY,
    first_name VARCHAR,
    last_name VARCHAR,
    phone VARCHAR,
    login VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    user_type VARCHAR NOT NULL DEFAULT 'user'
);