CREATE TABLE branch (
    id UUID PRIMARY KEY,
    code VARCHAR(3) NOT NULL,
    name VARCHAR NOT NULL,
    address VARCHAR,
    phone VARCHAR
);