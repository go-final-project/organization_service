CREATE TABLE provider (
    id UUID PRIMARY KEY,
    name VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    active VARCHAR NOT NULL DEFAULT 'active'
);